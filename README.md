# Pulicast Message Types

This repository contains the basic message types needed by Pulicast defined as [ZCM messages](https://github.com/ZeroCM/zcm/blob/master/docs/zcmtypesys.md). You can use the `zcm-gen` tool to generate class definitions from those message definitions. Read more about this in the [ZCM Tutorial](https://github.com/ZeroCM/zcm/blob/0efe35d1cd68f7a9f25902c4939496aecff3ebe0/docs/tutorial.md#hello-world-types).

For example to generate C++ headers from the message definitions in this repository use:

```bash
zcm-gen *.zcm --package-prefix pulicast_messages --cpp
```

